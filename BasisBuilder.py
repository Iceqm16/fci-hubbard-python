'''
Builds basis set for close shell system using strings of alpha and beta
electrons on a lattice.
'''
import global_fun as gf
import time
from input import *
try:
     from numba import autojit
     AJIT = True
except ImportError, e:
    AJIT = False
    pass # module doesn't exist, deal with it.
 

class Basis:


    def __init__(self, nstate, Na, Nb,fid):

        
        self.Na = Na
        self.Nb = Nb
        LT = self.BuildBasis(fid)
        self.LT = LT
    
    
    #@autojit
    def BuildBasis(self,fid):

        min_a = gf.MinI(self.Na)
        min_b = gf.MinI(self.Nb)
        max_a = gf.MaxI(self.Na)
        max_b = gf.MaxI(self.Nb)

        LT = []

        if True:

            print "\t\tBUILDING BASIS\n"
            print "\t\tSTARTING FROM MIN_A: %f" % min_a
            print "\t\tENDING AT MAX_A: %f" % max_a
            fid.write("\t\tBUILDING BASIS\n\n")
            fid.write("\t\tSTARTING FROM MIN_A: %f\n" % min_a)
            fid.write("\t\tENDING AT MAX_A: %f\n" % max_a)
 
            
            start_time = time.time()
            for i in range(min_a,max_a+1):

                la_good, _ = gf.DectoBin(Na,i)

                for j in range(min_b,max_b+1):

                    lb_good, _ = gf.DectoBin(Nb,j)
                    
                    if (lb_good == la_good and la_good == 1):
                        LT.append( int( (2**L)*i + j) )

            print "\t\tNUMBER OF STRINGS = %f" % len(LT)
            print "\t\tTIME TO BUILD BASIS = %f\n" % (time.time() - start_time)
            fid.write("\t\tNUMBER OF STRINGS = %f\n" % len(LT))
            fid.write("\t\tTIME TO BUILD BASIS = %f\n\n" % (time.time() - start_time) )



            #BUILD INTEGER REPRESENTATION OF EACH POSSIBLE STATE AND SORT!
            sorted(LT)
              
        return LT
                    

if __name__=="__main__":

    nn = 4**6
    
    Basis(nn,3,3)

    
