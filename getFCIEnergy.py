'''
Input is the directory

output is a list of U, gs energy

'''


import os
import sys


if __name__=="__main__":

    pdir = os.getcwd()
    os.chdir(sys.argv[1])

    dirs = filter(os.path.isdir, os.listdir('./'))

    U = []
    Energy =[]
    for dd in dirs:

        os.chdir(dd)

        
        with open("./fci.out",'r') as fid:

            line = fid.readline()
            while (1):

                if 'U =' in line:

                    U.append(float(line.split('=')[1]))

                if 'EIGENVALUES' in line:
                    line = fid.readline() 
                    Energy.append(float(line))
                    print U[-1], Energy[-1]

                    break

                line = fid.readline()

        os.chdir('../')

    os.chdir(pdir)
    with open('tmp.out','w') as fid:
        for xx in range(len(U)):

            fid.write("{: 2.2e}\t{: 3.10e}\n".format(U[xx],Energy[xx]))
