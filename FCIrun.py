'''
Run the FCI program


1) Build basis
2) Call Hamiltonian
3) diagonalize
4) print eigenvalues

'''
import os
import sys
sys.path.insert(0,'%s'% os.getcwd())
from input import *
import BasisBuilder as BB
import Hamiltonian as Ham
import Diagonalize as DD
import Diagonalize_2 as DD_2
import time



if __name__=="__main__":

    fid = open('fci.out','w')

    tdir = os.getcwd()
    start_time = time.time()
    #PRINT BANNER
    str1 = "\n\n\tFULL CONFIGURATION INTERACTION ON THE HUBBARD MODEL\n\tBY NICHOLAS C. RUBIN\n\n"
    print str1
    fid.write(str1)

    #STATE NUMBER
    nn = 4**L

    #PRINT VARIABLES
    print "\t\tU = %f" % U
    print "\t\tNa = %f" % Na
    print "\t\tNb = %f" % Nb
    print "\t\tt = %f" % t
    print "\t\tL = %f\n" % L
    fid.write("\t\tU = %f\n" % U )
    fid.write("\t\tNa = %f\n" % Na)
    fid.write("\t\tNb = %f\n" % Nb)
    fid.write("\t\tt = %f\n" % t)
    fid.write("\t\tL = %f\n" % L)


    #BUILD BASIS
    Basis_states = BB.Basis(nn,Na,Nb,fid)

    #BUILD HAMILTONIAN
    HH = Ham.Hamiltonian(Basis_states)

    #DIAGONALIZE
    #DD.Diagonalize(HH.Ham_Mat) 
    DD_2.Diagonalize(HH.Ham_Mat, Basis_states, fid)

    print "\n\n\n\t\tTOTAL TIME = %f" % (time.time() - start_time)
    print "\tSUCCESS!"
    fid.write("\n\n\t\tTOTAL TIME = %f\n" % (time.time() - start_time) )
    fid.write("\t\tSUCCESS! GOODBYE....\n")
    fid.close()
