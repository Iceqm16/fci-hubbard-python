'''
Calculate Gap from FCI energies.

'''

import sys
import os
import numpy as np

if __name__=="__main__":


    U = []
    ENp1 = []
    EN = []
    ENp2 = []
    Gap = []
    with open("./SingleParticleGap.txt",'r') as fid:

        for line in fid:
            
            if "#U" in line:
                pass
            else:
                line = filter(None,line.split(" "))
                if '\n' in line:
                    line.remove('\n')
                print line 
                    
                line = map(float,line)
                Gap.append((2*line[1] - line[2] - line[3]) )


    for xx in range(10):

        print xx, Gap[xx]/2.

