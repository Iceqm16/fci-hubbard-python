'''
Build the Hamiltonian given the basis string.

Builds a csr_matrix of the Hamiltonian to minimize memory requirements.
'''


from input import *
import BasisBuilder as BB
import global_fun as gf
import numpy as np
import time
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import eigsh
import sys
import matplotlib as ml
import matplotlib.pyplot as plt
import cPickle as pickle


try:
     from numba import autojit
     AJIT = True
except ImportError, e:
    AJIT = False
    pass # module doesn't exist, deal with it.
    

class Hamiltonian:

    def __init__(self,Basis):

        M = 2**L #THE BASIS NUMBER TO DETERMINE ALPHA/BETA STRINGS
        #Ham1 = np.zeros(( len(Basis.LT), len(Basis.LT) ) )
        #Ham = csr_matrix( (len(Basis.LT), len(Basis.LT) ), dtype=float)
        row = []
        col = []
        dat = []

        rowe = []
        cole = []
        date = []

        rowt = []
        colt = []
        datt = []
        
        xcount = 0
        ycount = 0
        print "\t\tBUILDING HAMILTONIAN"
        #fid.write("\t\tBUILDING HAMILTONIAN\n")
        start_time  = time.time()

        #ELINIMNATE DOTS SPEEDS UP FOR LOOPS
        #Hamiltonian = self.Hubbard_1D
        Hamiltonian = self.Hubbard_1D
        rappend = row.append
        cappend = col.append
        dappend = dat.append

        for xx in xrange(len(Basis.LT)):

            lax = Basis.LT[xx] % M
            lbx = Basis.LT[xx] / M
 
            for yy in xrange(xx,len(Basis.LT)):

                lay = Basis.LT[yy] % M
                lby = Basis.LT[yy] / M

                H_elem = Hamiltonian(lax,lbx,lay,lby, Basis.LT[xx],
                    Basis.LT[yy])
                if H_elem != 0 and xx != yy:
                    rappend(xx)
                    cappend(yy)
                    dappend(H_elem)
                    rappend(yy)
                    cappend(xx)
                    dappend(H_elem)
                if H_elem !=0 and xx == yy:
                    rappend(xx)
                    cappend(yy)
                    dappend(H_elem)

                   
        Ham = csr_matrix( (dat, (row,col)), shape=(len(Basis.LT),len(Basis.LT)))
        print "\t\tTIME TO BUILD HAMILTONIAN = %f\n" % (time.time() -\
                start_time)
        #fid.write("\t\tTIME TO BUILD HAMILTONIAN = %f\n" % (time.time() -\
        #        start_time) )
        self.Ham_Mat = Ham
        #print "\tGROUND STATE ENERGY FROM SPARSE = %f" % w[0]

   
        pickle.dump( Ham, open( "Ham.pkl", "wb" ) )


        #Ham = Ham.todense()
        #ww,vv = np.linalg.eigh(Ham)
        #sorted(ww,reverse=True)

        #print "\tGROUND STATE ENERGY = %f" %  ww[0]
        
        #plt.imshow(Ham)
        #plt.colorbar(orientation='vertical')
        #plt.show()
        #Tmat = Ham - Ham1
        #for xx in range(Tmat.shape[0]):
        #    for yy in range(Tmat.shape[1]):
        #        if Tmat[xx,yy] != 0:
        #            print Tmat[xx,yy]



 
    def Hubbard_1D(self, Ia_ket, Ib_ket, Ia_bra, Ib_bra, Iket, Ibra):

        #ELEMENT VALUE TO BE RETURNED AND VARIABLE DEFINITION
        H_elem = 0
        sign1 = 1
        sign2 = 1

        #PAIR LIST OPERATIONS

        _, ket_a = gf.DectoBin(Na,Ia_ket)
        #_, bra_a = gf.DectoBin(Na,Ia_bra)
        _, ket_b = gf.DectoBin(Nb,Ib_ket)
        #_, bra_b = gf.DectoBin(Nb,Ib_bra)

        #TWO BODY PART OF HAMILTONIAN
        if (Ia_ket == Ia_bra) and (Ib_ket == Ib_bra):

            for xx in range(len(ket_a)):

                if (ket_a[xx] == ket_b[xx]) and (ket_a[xx] == 1):
                    #print U
                    H_elem += U


        #ONE BODY PART OF HAMILTONIAN  sum_{<i,j>} c_{i}^{t}c_{j} | alpha,beta>

        #if Iket != Ibra:
        #    pass
            #print "State"
            #print "%s%s|%s%s" % ("".join(str(x) for x in bra_a), 
            #        "".join(str(x) for x in bra_b),
            #        "".join(str(x) for x in ket_a), 
            #        "".join(str(x) for x in ket_b) )


        for k in range(L):

            if ( k == 0):
                c = L -1
                
                ket_a_t = list(ket_a) #TRUE COPY USE list() or a[:]
                goodK,sign1 = gf.Killsign(k, ket_a_t)
                goodC,sign2 = gf.Createsign(c, ket_a_t)

                if goodK and goodC:

                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Iket_t = (2**L)*Ib_ket + Ia_ket_t

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

                ket_b_t = list(ket_b)
                goodK,sign1 = gf.Killsign(k, ket_b_t)
                goodC,sign2 = gf.Createsign(c, ket_b_t)

                if goodK and goodC:

                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + Ia_ket

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

            if k == (L - 1):
                c = 0
                ket_a_t = list(ket_a) #TRUE COPY USE list() or a[:]
                goodK,sign1 = gf.Killsign(k, ket_a_t)
                goodC,sign2 = gf.Createsign(c, ket_a_t)

                if goodK and goodC:

                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Iket_t = (2**L)*Ib_ket + Ia_ket_t

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

                ket_b_t = list(ket_b)
                goodK, sign1 = gf.Killsign(k, ket_b_t)
                goodC, sign2 = gf.Createsign(c, ket_b_t)

                if goodK and goodC:

                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + Ia_ket

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

            if k < (L - 1):
                c = k + 1
                ket_a_t = list(ket_a) #TRUE COPY USE list() or a[:]
                goodK, sign1 = gf.Killsign(k, ket_a_t)
                goodC, sign2 = gf.Createsign(c, ket_a_t)

                if goodK and goodC:

                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Iket_t = (2**L)*Ib_ket + Ia_ket_t

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

                ket_b_t = list(ket_b)
                goodK, sign1 = gf.Killsign(k, ket_b_t)
                goodC, sign2 = gf.Createsign(c, ket_b_t)

                if goodK and goodC:

                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + Ia_ket

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

            if k > 0:
                c = k - 1
                ket_a_t = list(ket_a) #TRUE COPY USE list() or a[:]
                goodK,sign1 = gf.Killsign(k, ket_a_t)
                goodC,sign2 = gf.Createsign(c, ket_a_t)
               
                if goodK and goodC:

                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Iket_t = (2**L)*Ib_ket + Ia_ket_t

                    if Iket_t == Ibra:
                        #print H_elem
                        H_elem += -1.*t*sign1*sign2
                        #print "moving alpha back one"
                        #print H_elem
 


                ket_b_t = list(ket_b)
                goodK,sign1 = gf.Killsign(k, ket_b_t)
                goodC,sign2 = gf.Createsign(c, ket_b_t)

                if goodK and goodC:

                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + Ia_ket

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2


        return H_elem




if __name__=="__main__":

    fid = open('test','w')
    Basis_states = BB.Basis(4**L,Na,Nb, fid)

    HH = Hamiltonian(Basis_states)
    Ham = HH.Ham_Mat.todense()
    w,v = np.linalg.eigh(Ham)

    print "GROUND STATE ENERGY ", w[0]

    fid.close()
