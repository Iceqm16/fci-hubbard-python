Hubbard FCI
by Nicholas C. Rubin
------

Rudimentary FCI on a Hubbard model on linear lattice.  Do not use in
production.  Also contains rudimentary contraction of FCI density to a 2-RDM.
