'''
This program computes the exact ground state given a set of integrals and a
Hamiltonian generator.  We reduce the size of the stored matrix by doing using
sparse storage and performing lanczos on the Hamiltonian to generate ground
state.

Things this program needs to do.

1) generate a basis 
2) evalueate the Hamiltonian given that basis
3) diagonalize.  

'''
import scipy as sp
from scipy.sparse.linalg import eigsh
from scipy.sparse import csr_matrix
import time
import numpy as np
import sys
sys.path.append('/home/nick/pyncr/ED')
import global_fun as gf
from input import *

class Diagonalize:
    
    
    def __init__(self, Mat, Basis, fid):
        '''
        Mat is a sparse matrix in csr format
        '''
        
        print "\n\t\tDIAGONALIZING HAMILTONIAN"
        fid.write("\n\t\tDIAGONALIZING HAMILTONIAN\n")
        start_time = time.time()
        #w,v = eigsh(Mat)
        try:
            w,v = eigsh(Mat, k = 21, mode='Buckling', which='SA')
            #w,v = np.linagl.eigh(Mat.todense())
            #w,v = np.linalg.eigh(np.array(Mat.todense()))
        except:
            try:
                print "\t\tFAILED FOR 1-VECTOR...RUNNING 2-VEC WITH BUCKLING"
                fid.write("\t\tFAILED FOR 1-VECTOR...RUNING 2-VECTOR WITH BUCKLING\n")
                w,v = eigsh(Mat, k = 2, mode='Buckling', which='SA')
            except:
                print "\t\tFAILED FOR 2-VECTOR. DOING DIRECT CALCULATION"
                fid.write("\t\tFAILD FOR 2-VECTOR. DOING DIRECT CALC\n")
                Mat = Mat.todense()
                w,v = np.linalg.eigh(Mat)

        self.eigenvalues = w
        self.eigenvectors = v

        print "\n\t\tGROUND STATE ENERGY = %f" % w[0]
        fid.write("\n\t\tGROUND STATE ENERGY = %f\n" % w[0])
        print "\t\tTIME = %f" % (time.time() - start_time)
        fid.write("\t\tTIME= %f\n" % (time.time() - start_time))

        fid.write("\nEIGENVALUES\n")
        for i in range(len(w)):
            fid.write("{: 5.10e}\n".format(w[i]) )


        M = 2**L


        for k in range(v.shape[1]):
            fid.write("\nNONZERO COEFFICIENTS FOR STATE = %i\n" % k)
            for i in range(v[:,k].shape[0]):

                lax = Basis.LT[i] % M
                lbx = Basis.LT[i] / M

                _, ket_a = gf.DectoBin(Na, lax)
                _, ket_b = gf.DectoBin(Nb, lbx)

                ket = ''.join(str(x) for x in ket_a)
                ket += '|' + ''.join(str(x) for x in ket_b)

                if np.abs(v[i,k]) > 1.0E-15:
                    fid.write("{: 5.10e}\t{tket}\t{bas_num}\n".format(v[i,k],
                        tket=ket, bas_num=i) )


        
if __name__=="__main__":

    #Create a square matrix and covert it to a sparse matrix

    M = 5000

    Mat = np.zeros((M,M))
    for xx in range(M):
        for yy in range(M):

            if ((xx%2) == xx) or ((xx%3)  == xx):
                pass
            else:
                Mat[xx,yy] = np.random.rand()

    Mat = (Mat + Mat.T)/2.
    Mat = csr_matrix(Mat)
    Diagonalize(Mat)
