from input import *
import numpy as np

def MinI(N):

    min = 0
    for i in range(N):

        if (i < L):

            min += 2**i

    return min


def MaxI(N):


    max = 0
    for i in range(N):

        site = L - i - 1
        if site >=0:
            max += 2**site

    return max


def DectoBin(Na, n):


    bin_ = [0]*L
    #bin_ = np.zeros(L)
    cnt =0

    for i in range(L):
        bin_[L-i-1]=n%2
        if (n%2==1): 
            cnt += 1
        n=n/2
        if (n==0): 
            break

    #print bin_
    if (cnt==Na): 
        return 1, bin_
    else:
        return 0, bin_

def bitLen(a):
    #a is a bit string
    
    return len(bin(a)) - 2

def bitCountLen(a):

    count = 0
    for x in bin(a)[2:]:
        if x == '1': count += 1

    return count

def ItfromVec(vec):

    Itmp = 0
    for i in range(L):
        Itmp += vec[i]*(2**(L-i-1))

    #Itmp = sum(map(lambda x: vec[x]*(2**L-x-1),range(len(vec))))

    return Itmp

def Killsign(k, vec):

    s = 1

    if vec[k] == 0:
        sign = s
        good = 0

    else:
        for i in range(k):
            if vec[i] == 1:
                s *= -1

        #if sum(vec[:k])%2 == 0:
        #    s_t = 1
        #else:
        #    s_t = -1

        #assert(s == s_t)
        vec[k] = 0
        sign = s
        good = 1

    return good, sign

def Createsign(k,vec):

    s = 1
    #print "creating at %i" % k
    #print vec[k]
    if vec[k] == 1:
        sign = s
        good = 0

    else:
        for i in range(k):
            if vec[i] == 1:
                s *= -1

        #if sum(vec[:k])%2 == 0:
        #    s_t = 1
        #else:
        #    s_t = -1

        #assert(s == s_t)
        vec[k] = 1
        sign = s
        good = 1

    return good, sign



if __name__=="__main__":

    #DectoBin(3,11)
    sign1 = 1
    sign2 = 1
    vec = [0,0,0,1,1,1]
    num = ItfromVec(vec)
    print num
    #print vec
    #goodK = Killsign(0,vec,sign1)
    #goodC = Createsign(L-1,vec,sign2)
    #print vec
    #print goodK
    #print goodC


