'''
Contract FCI wave function to density matrices.  

return object that contains 1 and 2 particle density matrices
 

'''
from input import *
import time
import BasisBuilder as BB
import global_fun as gf
import Hamiltonian as Ham
import Diagonalize as DD
import numpy as np
from scipy.sparse import csr_matrix
import sys

class FCI2RDM:

    def __init__(self, Diag, Basis,fid):

        #EVALUATE D1a by <\Psi|a_{i}^{t}a_{j}|\Psi>

        #BUILD NONZERO WF COEFFICIENTS
        M = 2**L

        #FIND ALL DEGENERATE EIGENVECTORS
        cnt = 1
        for ww in Diag.eigenvalues[1:]:
            if ww == Diag.eigenvalues[0]:
                cnt += 1

        gs = np.zeros(Diag.eigenvectors[:,0].shape)
        for ii in range(cnt):
            gs += Diag.eigenvectors[:,ii]

        #gs = Diag.eigenvectors[:,0] + Diag.eigenvectors[:,1]
        gs = gs*1./np.linalg.norm(gs)
        CI = []
        It = []

        #CREATE DICTIONARY--i.e. HASHTABLE TO SPEEDUP LOOKUPS
        strings = {}
        stringsr = {}
        for i in range(len(gs)):

            lax = Basis.LT[i] % M
            lbx = Basis.LT[i] / M
            _, ket_a = gf.DectoBin(Na, lax)
            _, ket_b = gf.DectoBin(Nb, lbx)
            strings[lbx] = ket_b
            strings[lax] = ket_a
            stringsr[tuple(ket_a)] = lax
            stringsr[tuple(ket_b)] = lbx


            if np.abs(gs[i]) > 1.0E-15:
                CI.append(gs[i])
                It.append(Basis.LT[i])


        D1a = self.BuildD1a(CI, It, strings)
        D2aa = self.BuildD2aa(CI, It, strings)
        D2ab = self.BuildD2ab(CI,It, strings)

        
        np.save("D1a",D1a)
        np.save("D2aa",D2aa)
        np.save("D2ab",D2ab)

        print "\n\t\tTRACE of N-D = %f" % (np.trace(np.outer(gs,gs)))
        print "\t\tDIM OF D1 = [%i,%i]" % (D1a.shape[0], D1a.shape[1])
        print "\t\tDIM OF D2aa = [%i,%i]" % (D2aa.shape[0], D2aa.shape[1])
        print "\t\tDIM OF D2ab = [%i,%i]" % (D2ab.shape[0], D2ab.shape[1])
        print "\t\tTRACE OF D2aa = %f" % (np.trace(D2aa))
        print "\t\tTRACE OF D2aa = %f" %  (np.trace(D2ab))


        w,v = np.linalg.eigh(D1a)
        fid.write("\nD1a EIGENVALUES\n")
        for eig in w:
            fid.write("\t{: 2.10e}\n".format(eig))

        w,v = np.linalg.eigh(D2aa)
        fid.write("\nD2aa EIGENVALUES\n")
        for eig in w:
            fid.write("\t{: 2.10e}\n".format(eig))

        w,v = np.linalg.eigh(D2ab)
        fid.write("\nD2ab EIGENVALUES\n")
        for eig in w:
            fid.write("\t{: 2.10e}\n".format(eig))

        #fid.write("\n\tD1a ELEMENTS\n")
        #for xx in range(D1a.shape[0]):
        #    for yy in range(D1a.shape[1]):
        #        fid.write("{: 2.10e}\n".format(D1a[xx,yy]))

        #fid.write("\n\tD2aa ELEMENTS\n")
        #for xx in range(D2aa.shape[0]):
        #    for yy in range(D2aa.shape[1]):
        #        fid.write("{: 2.10e}\n".format(D2aa[xx,yy]))

        #fid.write("\n\tD2ab ELEMENTS\n")
        #for xx in range(D2ab.shape[0]):
        #    for yy in range(D2ab.shape[1]):
        #        fid.write("{: 2.10e}\n".format(D2ab[xx,yy]))




    def BuildD2ab(self,CI, It,strings):

        dim = int(L*L) #BASIS i < j no such thing as
        #a_{i}^{dagg}a_{i}^{dagg}
        M = 2**L
        D2ab = np.zeros((dim,dim))

        #BUILD BASIS FOR D2aa
        D2abBas = {}
        cnt = 0
        for r in range(L):
            for z in range(L):
                #print "%i,%i" % (r+1,z+1)
                D2abBas[cnt] = (r,z)
                cnt += 1

        for i in xrange(D2ab.shape[0]):
            for j in xrange(i,D2ab.shape[1]):

                for k in range(len(CI)):

                    Iket_a = It[k] % M
                    Iket_b = It[k] / M
                    #_, ket_a = gf.DectoBin(Na, Iket_a)
                    #_, ket_b = gf.DectoBin(Nb, Iket_b)
                    ket_a = strings[Iket_a]
                    ket_b = strings[Iket_b]

                    ket_a_t = list(ket_a)
                    ket_b_t = list(ket_b)
                    
                    goodK1, sign1 = gf.Killsign(D2abBas[j][0], ket_a_t)
                    goodK2, sign2 = gf.Killsign(D2abBas[j][1], ket_b_t)
                    goodC1, sign3 = gf.Createsign(D2abBas[i][0], ket_a_t)
                    goodC2, sign4 = gf.Createsign(D2abBas[i][1], ket_b_t)
                    if goodK1 and goodK2 and goodC1 and goodC2:
                        Ia_ket_t = gf.ItfromVec(ket_a_t)
                        Ib_ket_t = gf.ItfromVec(ket_b_t)
                        Iket_t = (2**L)*Ib_ket_t + Ia_ket_t
                        try:
                            l = It.index(Iket_t)
                            if i == j:
                                D2ab[i,j] += CI[k]*CI[l]
                            else:
                                D2ab[i,j] += \
                                CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                D2ab[j,i] += \
                                CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                #print D2ab[i,j]
                        except ValueError:
                            pass


                        #for l in xrange(len(CI)):
                        #    if Iket_t == It[l]:
                        #        if i == j:
                        #            D2ab[i,j] += CI[k]*CI[l]
                        #        else:
                        #            D2ab[i,j] += \
                        #            CI[k]*sign1*sign2*sign3*sign4*CI[l]
                        #            D2ab[j,i] += \
                        #            CI[k]*sign1*sign2*sign3*sign4*CI[l]
                        #            #print D2ab[i,j]

        return D2ab



    def BuildD2aa(self,CI, It, strings):

        dim = int(L*(L - 1)/2.) #BASIS i < j no such thing as
        #a_{i}^{dagg}a_{i}^{dagg}
        M = 2**L
        D2aa = np.zeros((dim,dim))

        #BUILD BASIS FOR D2aa
        D2aaBas = {}
        cnt = 0
        for r in range(L):
            for z in range(r+1,L):
                #print "%i,%i" % (r+1,z+1)
                D2aaBas[cnt] = (r,z)
                cnt += 1
        
        for i in xrange(D2aa.shape[0]):
            for j in xrange(i,D2aa.shape[1]):

                for k in xrange(len(CI)):

                    Iket_a = It[k] % M
                    Iket_b = It[k] / M
                    ket_a = strings[Iket_a]
                    ket_b = strings[Iket_b]
                    #_, ket_a = gf.DectoBin(Na, Iket_a)
                    #_, ket_b = gf.DectoBin(Nb, Iket_b)
                    ket_a_t = list(ket_a)
                    #print "<%i,%i|%i%i>" % (D2aaBas[j][0], D2aaBas[j][1], 
                    #        D2aaBas[i][0], D2aaBas[i][1])
                    goodK1, sign1 = gf.Killsign(D2aaBas[j][0], ket_a_t)
                    goodK2, sign2 = gf.Killsign(D2aaBas[j][1], ket_a_t)
                    goodC1, sign3 = gf.Createsign(D2aaBas[i][0], ket_a_t)
                    goodC2, sign4 = gf.Createsign(D2aaBas[i][1], ket_a_t)
  
                    if goodK1 and goodK2 and goodC1 and goodC2:
                        Ia_ket_t = gf.ItfromVec(ket_a_t)
                        Iket_t = (2**L)*Iket_b + Ia_ket_t
                        
                        try:
                            l = It.index(Iket_t)
                            if i == j:
                                D2aa[i,j] += CI[k]*CI[l]  
                            else:
                                D2aa[i,j] += \
                                CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                D2aa[j,i] += \
                                CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                #print D2aa[i,j]
                        except ValueError:
                            pass

                        #for l in xrange(len(CI)):
                        #    if Iket_t == It[l]:
                        #        if i == j:
                        #            D2aa[i,j] += CI[k]*CI[l]  
                        #        else:
                        #            D2aa[i,j] += \
                        #            CI[k]*sign1*sign2*sign3*sign4*CI[l]
                        #            D2aa[j,i] += \
                        #            CI[k]*sign1*sign2*sign3*sign4*CI[l]
                        #            #print D2aa[i,j]

        return D2aa

   
    def BuildD1a(self, CI, It, strings ):

        #PRELIMINARIES FOR EXTRACTING STRINGS AS USUAL
        M = 2**L

        #EACH KILLER CREATOR ACTS AT EACH NONZERO COEFFICIENT
        #FOR DIAGONAL ELEMENTS JUST CHECK IF 1 IS THERE. AND IF BETA STRING IS
        #THE SAME

        #FOR OFF DIAGONAL ELEMENTS WE NEED TO CREATE KILL, PUT INTO CANONICAL
        #ORDERING, AND CHECK BETA STRING.  #ALPHA BLOCK DOES NOT TALK TO BETA
        #BLOCK
        D1a = np.zeros((L,L))
        for i in xrange(L):
            for j in xrange(i,L): #UPPER TRIANGLE
                #print "%i,%i" % (i,j)
                
                #LOOP OVER ENTIRE CI VECTOR KILL AT EACH
                for k in xrange(len(CI)):

                    lax = It[k] % M
                    lbx = It[k] / M

                    ket_a = strings[lax]
                    ket_b = strings[lbx]
                    #_, ket_a = gf.DectoBin(Na, lax)
                    #_, ket_b = gf.DectoBin(Nb, lbx)

                    #ket = ''.join(str(x) for x in ket_a)
                    #ket += '|' + ''.join(str(x) for x in ket_b)

                    #print "%s" % ket

                    ket_a_t = list(ket_a)
                    goodK, sign1 = gf.Killsign( j, ket_a_t)
                    goodC, sign2 = gf.Createsign( i, ket_a_t)
                    if goodK and goodC:
                        Ia_ket_t = gf.ItfromVec(ket_a_t)
                        Iket_t = (2**L)*lbx + Ia_ket_t
                        try:
                            l = It.index(Iket_t)
                            if i == j:
                                D1a[i,j] += CI[k]*CI[k]
                            else:
                                D1a[i,j] += CI[k]*sign1*sign2*CI[l]
                                D1a[j,i] += CI[k]*sign1*sign2*CI[l]

                        except ValueError:
                            pass
                            #print Iket_t
                            #print "lbx = %i" %lbx
                            #print "Ia_ket_t = %i" %Ia_ket_t
                            #print strings[lbx]
                            #print strings[Ia_ket_t]

                        #for l in xrange(len(CI)): #OKAY CHECK BRA's NOW FOR
                        #    #EQUIVALENCY
                        #    #print "%i =? %i" % (It[l], Iket_t) 
                        #    if Iket_t == It[l]: #WE FOUND A MATCHING STRING
                        #        #print "YES"                  
                        #        if i == j:
                        #            D1a[i,j] += CI[k]*CI[k]
                        #        else:
                        #            D1a[i,j] += CI[k]*sign1*sign2*CI[l]
                        #            D1a[j,i] += CI[k]*sign1*sign2*CI[l]

        return D1a

if __name__=="__main__":

    #Debug we need to Build Basis
    #Build Hamiltonian
    #Diagonalize
    #then call this routine.
    
    fid = open('test.log','w')
    #BUILD BASIS
    Basis_states = BB.Basis(4**L,Na,Nb, fid)
    #BUILD HAMILTONIAN
    HH = Ham.Hamiltonian(Basis_states, fid)
    #DIAGONALIZE
    Diag = DD.Diagonalize(HH.Ham_Mat, Basis_states, fid) 
    
    FCI2RDM(Diag,Basis_states, fid)

    fid.close()
