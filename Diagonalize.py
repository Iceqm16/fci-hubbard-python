'''
This program computes the exact ground state given a set of integrals and a
Hamiltonian generator.  We reduce the size of the stored matrix by doing using
sparse storage and performing lanczos on the Hamiltonian to generate ground
state.

Things this program needs to do.

1) generate a basis 
2) evalueate the Hamiltonian given that basis
3) diagonalize.  

'''
import scipy as sp
from scipy.sparse.linalg import eigsh
from scipy.sparse import csr_matrix
import time
import numpy as np
from input import *

class Diagonalize:
    
    
    def __init__(self, Mat):
        '''
        Mat is a sparse matrix in csr format
        '''
        
        print "\n\t\tDIAGONALIZING HAMILTONIAN"
        start_time = time.time()
        #w,v = eigsh(Mat)
        w,v = eigsh(Mat, k = 6, mode='buckling', which='SA')
        print "\n\t\tGROUND STATE ENERGY = %f" % w[0]
        print "\t\tTIME = %f" % (time.time() - start_time)

        fid = open("fci.out",'w')
        fid.write("EIGENVALUES\n")
        for i in range(len(w)):
            fid.write("%e\n" % w[i])
        fid.close()

        
if __name__=="__main__":

    #Create a square matrix and covert it to a sparse matrix

    M = 5000

    Mat = np.zeros((M,M))
    for xx in range(M):
        for yy in range(M):

            if ((xx%2) == xx) or ((xx%3)  == xx):
                pass
            else:
                Mat[xx,yy] = np.random.rand()

    Mat = (Mat + Mat.T)/2.
    Mat = csr_matrix(Mat)
    Diagonalize(Mat)
