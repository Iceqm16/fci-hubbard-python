'''
Read FCI vector and contract to ab-2-RDM.  For closed shell the antisymmetric
piece will be equivalent to the D2aa

1) Build Basis states
2) Read fci.out file
3) converge
'''


import BasisBuilder as BB
import global_fun as gf
from scipy.sparse import csr_matrix
import sys
import os
from glob import glob
import numpy as np
import time

def ReadInput(infile):

    with open(infile,'r') as fid:

        for line in fid:

            if "L =" in line:
                L = int(line.split('=')[1])
            if "Na =" in line:
                Na = int(line.split('=')[1])
            if "Nb =" in line:
                Nb = int(line.split('=')[1])
            if "U =" in line:
                U = float(line.split('=')[1])

    return L, Na, Nb, U

def GetHashedStrings(Basis, Na, Nb):

    M = 2**L
    strings = {}
    stringsr = {}
    for i in range(len(Basis.LT)):

        lax = Basis.LT[i] % M
        lbx = Basis.LT[i] / M
        _, ket_a = gf.DectoBin(Na, lax)
        _, ket_b = gf.DectoBin(Nb, lbx)
        strings[lbx] = ket_b
        strings[lax] = ket_a
        stringsr[tuple(ket_a)] = lax
        stringsr[tuple(ket_b)] = lbx

    
    LTdict = dict(zip( range(len(Basis.LT)) , Basis.LT) )

    return strings, stringsr, LTdict

def ReadOutput(outfile, Na, Nb, LTdict, LTdict_rev, Basis):

    CIcoeff = np.zeros( (len(Basis.LT),1), dtype=float )
    with open(outfile,'r') as fid:

        line = ' '
        while 'STATE = 0' not in fid.readline():
            pass

        while 'STATE = 1' not in line:
            
            try:
                line = fid.readline()
                line = filter(None, line.split('\t'))
                CIcoeff[ int(line[2]) ] = float(line[0])
            except:
                break
  
    return CIcoeff

def BuildD2ab(CI, It, strings):

    dim = int(L*L) #BASIS i < j no such thing as
    #a_{i}^{dagg}a_{i}^{dagg}
    M = 2**L
    D2ab = np.zeros((dim,dim))

    #BUILD BASIS FOR D2aa
    D2abBas = {}
    cnt = 0
    for r in range(L):
        for z in range(L):
            #print "%i,%i" % (r+1,z+1)
            D2abBas[cnt] = (r,z)
            cnt += 1

    for i in xrange(D2ab.shape[0]):
        for j in xrange(i,D2ab.shape[1]):
            start_time = time.time()
            print i,j, " of ", D2ab.shape
            for k in range(len(CI)):

                Iket_a = It[k] % M
                Iket_b = It[k] / M
                #_, ket_a = gf.DectoBin(Na, Iket_a)
                #_, ket_b = gf.DectoBin(Nb, Iket_b)
                ket_a = strings[Iket_a]
                ket_b = strings[Iket_b]
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
               
                #D2(ij,k,l) Phys notation = a^{t}_{i}a^{t}_{j}a_{l}a_{k}
                goodK1, sign1 = gf.Killsign(D2abBas[j][0], ket_a_t)
                goodK2, sign2 = gf.Killsign(D2abBas[j][1], ket_b_t)
                goodC1, sign3 = gf.Createsign(D2abBas[i][0], ket_a_t)
                goodC2, sign4 = gf.Createsign(D2abBas[i][1], ket_b_t)

                if goodK1 and goodK2 and goodC1 and goodC2:
                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + Ia_ket_t
                    try:
                        l = It.index(Iket_t)
                        if i == j:
                            D2ab[i,j] += CI[k,0]*CI[l,0]
                        else:
                            D2ab[i,j] += \
                            CI[k,0]*sign1*sign2*sign3*sign4*CI[l,0]
                            D2ab[j,i] += \
                            CI[k,0]*sign1*sign2*sign3*sign4*CI[l,0]
                    except ValueError:
                        pass
            
            print time.time() - start_time

    return D2ab


if __name__=="__main__":

    fid = open("logfile.log",'w')
    infile = glob('./input.py')[0]
    outfile = glob('./fci.out')[0]

    L, Na, Nb, U = ReadInput(infile)

    Basis = BB.Basis(4**L, Na, Nb, fid) 

    strings, stringsr, LTdict = GetHashedStrings(Basis, Na, Nb)
    LTdict_rev = dict(zip( LTdict.values(), LTdict.keys() ) )

    CIcoeff = ReadOutput(outfile, Na, Nb, LTdict, LTdict_rev, Basis) 

    trace = 0
    for xx in xrange(CIcoeff.shape[0]):
        trace += CIcoeff[xx,0]**2

    assert( np.abs(1.0 - trace) < float(1.0E-10) )

    D2ab = BuildD2ab(CIcoeff, Basis.LT , strings)

    np.save("D2ab.npy",D2ab)

    fid.close()
